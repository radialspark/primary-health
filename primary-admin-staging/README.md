### Command to send application for build (replace path to source blob)
```
curl -n -X POST https://api.heroku.com/app-setups \
  -H "Content-Type: application/json" \
  -H "Accept: application/vnd.heroku+json; version=3" \
  -d '{
  "app": {
    "name": "primary-admin-staging-ps",
    "space": "primary-health"
  },
  "source_blob": {
    "url": "https://bitbucket.org/radialspark/primary-health/get/primary-admin-staging.tar.gz"
  },
  "overrides": {
    "env": {
      "CAIR_PASSWORD": "test",
      "CAIR_USERNAME": "test",
      "CAIR_WSDL": "test",
      "CCRS_CER_S3_PATH": "test",
      "CCRS_ENDPOINT": "test",
      "CCRS_KEY_S3_PATH": "test",
      "CIIS_PASSWORD": "test",
      "DB_POOL": "test",
      "DD_AGENT_MAJOR_VERSION": "test",
      "DD_API_KEY": "test",
      "DD_LOG_LEVEL": "test",
      "LANG": "test",
      "MAILTRAP_API_TOKEN": "test",
      "MEDUSA_PASSWORD": "test",
      "MIIC_PASSWORD": "test",
      "MIIC_USERNAME": "test",
      "NEW_RELIC_APP_NAME": "test",
      "NODE_ENV": "test",
      "PGHERO_PASSWORD": "test",
      "PGHERO_USERNAME": "test",
      "QUOTAGUARDSTATIC_URL": "test",
      "RACK_ENV": "test",
      "RAILS_ENV": "test",
      "RAILS_LOG_TO_STDOUT": "test",
      "RAILS_MASTER_KEY": "test",
      "RAILS_MAX_THREADS": "test",
      "RAILS_SERVE_STATIC_FILES": "test",
      "RATE_LIMIT_PER_TEST_GROUP": "test",
      "REDIS_URL": "test",
      "REDISCLOUD_URL": "test",
      "REDOX_API_KEY": "test",
      "REDOX_PRODUCTION": "test",
      "REDOX_SECRET": "test",
      "REDOX_VERIFICATION_TOKEN": "test",
      "RELEASE_SKIP_MIGRATIONS": "test",
      "SECRET_KEY_BASE": "test",
      "SENTRY_DSN": "test",
      "STRIPE_CONNECT_CLIENT_ID": "test",
      "STRIPE_PUBLISHABLE_KEY": "test",
      "STRIPE_SECRET_KEY": "test",
      "UNLOCK_GLOBAL_CASE_REPORT": "test",
      "WEB_CONCURRENCY": "test",
      "WKHTMLTOPDF_URL": "test"
    }
  }
}'
```