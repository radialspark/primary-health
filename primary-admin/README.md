### Command to send application for build (replace path to source blob)
```

curl -n -X POST https://api.heroku.com/app-setups \
  -H "Content-Type: application/json" \
  -H "Accept: application/vnd.heroku+json; version=3" \
  -d '{
  "app": {
    "name": "primary-admin-ps",
    "space": "primary-health"
  },
  "source_blob": {
    "url": "https://bitbucket.org/radialspark/primary-health/raw/32278de90c709883caee77812d153dfc5c8c2924/primary-admin/primary-admin.tar.gz"
  },
  "overrides": {
    "env": {
      "__RAILS_ASSET_HOST": "test",
      "CAIR_PASSWORD": "test",
      "CAIR_USERNAME": "test",
      "CAIR_WSDL": "test",
      "CCHS_PASSWORD": "test",
      "CCHS_URL": "test",
      "CCHS_USERNAME": "test",
      "CCRS_CER_S3_PATH": "test",
      "CCRS_ENDPOINT": "test",
      "CCRS_KEY_S3_PATH": "test",
      "CIIS_PASSWORD": "test",
      "DB_POOL": "test",
      "DD_AGENT_MAJOR_VERSION": "test",
      "DD_API_KEY": "test",
      "DD_DYNO_HOST": "test",
      "DD_LOG_LEVEL": "test",
      "ENABLE_PGHERO": "test",
      "ENFORCE_INACTIVITY_TIMEOUT": "test",
      "HOST_DOMAIN": "test",
      "LANG": "test",
      "LIGO_SFTP_PASSWORD": "test",
      "LOOKER_USE_URL": "test",
      "MAYOLINK_HOST": "test",
      "MAYOLINK_PASSWORD": "test",
      "MAYOLINK_USERNAME": "test",
      "MEDUSA_PASSWORD": "test",
      "MIIC_FACILITY_ID": "test",
      "MIIC_PASSWORD": "test",
      "MIIC_USERNAME": "test",
      "MIIC_WSDL": "test",
      "NEW_RELIC_APP_NAME": "test",
      "PGHERO_PASSWORD": "test",
      "PGHERO_USERNAME": "test",
      "QUOTAGUARDSTATIC_URL": "test",
      "RACK_ATTACK_ENABLED": "test",
      "RACK_ENV": "test",
      "RAILS_ENV": "test",
      "RAILS_LOG_TO_STDOUT": "test",
      "RAILS_MASTER_KEY": "test",
      "RAILS_MAX_THREADS": "test",
      "RAILS_SERVE_STATIC_FILES": "test",
      "RATE_LIMIT_PER_TEST_GROUP": "test",
      "REDIS_URL": "test",
      "REDOX_API_KEY": "test",
      "REDOX_PRODUCTION": "test",
      "REDOX_SECRET": "test",
      "REDOX_VERIFICATION_TOKEN": "test",
      "RELEASE_SKIP_MIGRATIONS": "test",
      "SECRET_KEY_BASE": "test",
      "SENTRY_DSN": "test",
      "SENTRY_DSN_OLD": "test",
      "SIDEKIQ_PASSWORD": "test",
      "SIDEKIQ_USER": "test",
      "STRIPE_CONNECT_CLIENT_ID": "test",
      "STRIPE_CONNECT_SIGNING_SECRET": "test",
      "STRIPE_PUBLISHABLE_KEY": "test",
      "STRIPE_SECRET_KEY": "test",
      "STRIPE_SIGNING_SECRET": "test",
      "SyEvN0Gw": "test",
      "THROTTLING_RATE_REQUESTS": "test",
      "UNLOCK_GLOBAL_CASE_REPORT": "test",
      "WEB_CONCURRENCY": "test",
      "WKHTMLTOPDF_DOWNLOAD_URL": "test"
    }
  }
}'
```